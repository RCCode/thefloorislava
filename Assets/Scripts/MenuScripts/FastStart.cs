﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastStart : MonoBehaviour
{
    [SerializeField] LevelChanger levelChanger;
    [SerializeField] string inputString = "Jump";

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButton(inputString))
        {
            levelChanger.FadeToLevel(Scene.LevelScene);
        }
    }
}
