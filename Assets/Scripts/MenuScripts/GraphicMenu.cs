﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicMenu : MonoBehaviour
{
    [SerializeField] private Dropdown resolutionDropdown;
    [SerializeField] private Dropdown graphicsDropdown;

    Resolution[] resolutions;

    List<string> graphics;

    // Start is called before the first frame update
    void Start()
    {
        //Instantiate all possible graphic Qualities
        graphics = new List<string>() { "Very Low", "Low", "Medium", "High", "Very High", "Ultra" };

        GetResolution();
        GetGraphic();
    }

    /// <summary>
    /// Put available Screen Resolutions into the dropdown
    /// </summary>
    private void GetResolution()
    {
        List<string> options = new List<string>();
        int currentResIndex = 0;

        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        for (int i = 0; i < resolutions.Length; i++)
        {
            options.Add(resolutions[i].ToString().ToLower());

            if (resolutions[i].ToString().Equals(Screen.currentResolution.ToString()))
            {
                currentResIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResIndex;
        resolutionDropdown.RefreshShownValue();
    }

    /// <summary>
    /// Put available Qualitylevel into Dropdown
    /// </summary>
    private void GetGraphic()
    {
        graphicsDropdown.ClearOptions();

        graphicsDropdown.AddOptions(graphics);

        graphicsDropdown.value = QualitySettings.GetQualityLevel();
        graphicsDropdown.RefreshShownValue();
    }

    /// <summary>
    /// Set resolution on changed value
    /// </summary>
    public void SetResolution(int resolutionIndex)
    {
        Screen.SetResolution(resolutions[resolutionIndex].width, resolutions[resolutionIndex].height, Screen.fullScreen);
    }

    /// <summary>
    /// Set graphicQuality on changed value
    /// </summary>
    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex, true);
    }

    /// <summary>
    /// Set Fullscreen true/false
    /// </summary>
    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

}
