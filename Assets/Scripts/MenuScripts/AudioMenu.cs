﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioMenu : MonoBehaviour
{
    [SerializeField] private AudioMixer masterMixer;
    [SerializeField] private Slider[] volumeSlider;


    string[] volumeStrings;
    // Start is called before the first frame update
    void Start()
    {
        volumeStrings = new string[] { "MasterVolume", "MusicVolume", "SFXVolume" };

        for (int i = 0; i < volumeStrings.Length; i++)
        {
            GetVolume(i);
        }
    }

    /// <summary>
    /// Put current Volume into slider
    /// </summary>
    private void GetVolume(int index)
    {
        float currentVolume = 0;

        if (masterMixer.GetFloat(volumeStrings[index], out currentVolume))
        {
            volumeSlider[index].value = currentVolume;
        }
    }

    /// <summary>
    /// Set the Volume on changed value for MainMixer
    /// </summary>
    public void SetMasterVolume(float volume)
    {
        masterMixer.SetFloat(volumeStrings[0], volume);
    }

    /// <summary>
    /// Set the Volume on changed value for Music Mixer
    /// </summary>
    public void SetMusicVolume(float volume)
    {
        masterMixer.SetFloat(volumeStrings[1], volume);
    }

    /// <summary>
    /// Set the Volume on changed value for SFX Mixer
    /// </summary>
    public void SetSFXVolume(float volume)
    {
        masterMixer.SetFloat(volumeStrings[2], volume);
    }
}
