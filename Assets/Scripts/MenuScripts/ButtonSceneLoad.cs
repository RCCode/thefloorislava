﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Loads the selected Level OnButtonClick Event
/// </summary>
/// <remarks>
/// Attach this Script to every Button which loads new Scenes
/// or Quits the Application
/// </remarks>
[RequireComponent(typeof(Button))]
public class ButtonSceneLoad : MonoBehaviour
{
    [SerializeField] LevelChanger levelChanger;

    [Space]
    [SerializeField] Scene sceneToLoad;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => levelChanger.FadeToLevel(sceneToLoad));
    }
}
