﻿using UnityEngine;

public class IngameMenu : MonoBehaviour
{
    [SerializeField] GameObject ingameMenu;
    [SerializeField] string menuInputString = "Cancel";
    [SerializeField] float audioPitch = 0.6f;

    [Space]
    [SerializeField] Timer timer;
    [SerializeField] CharacterController2D characterController;

    bool menuOpen = false;
    float originalPitch;

    // Start is called before the first frame update
    void Start()
    {
        ingameMenu.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown(menuInputString))
        {
            if (!menuOpen)
            {
                OpenMenu();
            }
            else
            {
                CloseMenu();
            }
        }
    }

    public void OpenMenu()
    {
        ingameMenu.SetActive(true);
        characterController.enabled = false;

        Time.timeScale = 0;
        timer.StopTimer();

        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;

        if (AudioManager.instance != null)
        {
            originalPitch = AudioManager.instance.originalSource.pitch;
            AudioManager.instance.originalSource.pitch = audioPitch;
        }

        menuOpen = true;
    }

    public void CloseMenu()
    {
        Time.timeScale = 1;
        timer.ResumeTimer();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        menuOpen = false;

        ingameMenu.SetActive(false);
        characterController.enabled = true;

        if (AudioManager.instance != null)
        {
            AudioManager.instance.originalSource.pitch = originalPitch;
        }
    }
}
