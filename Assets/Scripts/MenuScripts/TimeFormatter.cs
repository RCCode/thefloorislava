﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class TimeFormatter
{
    public static string FormatMinutes(float time, string format = "00")
    {
        return ((int)time / 60).ToString(format);
    }

    public static string FormatSeconds(float time, string format = "00")
    {
        return ((int)time % 60).ToString(format);
    }

    public static string FormatMilliSeconds(float time, string format = "000")
    {
        return ((time * 1000f) % 1000).ToString(format);
    }
}

