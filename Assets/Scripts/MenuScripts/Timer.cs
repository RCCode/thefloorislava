﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [HideInInspector]
    public Stopwatch stopwatch;

    [SerializeField] TextMeshProUGUI textField;
    [SerializeField] TextMeshProUGUI milliSecTextField;

    [Space]
    [Range(0,0.5f)]
    [SerializeField] float pitchStep = 0.05f;

    bool timerStop = false;
    float pitchCount = 0;

    void Start()
    {
        stopwatch = new Stopwatch();
        stopwatch.Start();
    }


    void FixedUpdate()
    {
        textField.text = string.Join("X", stopwatch.Elapsed.Minutes.ToString("00"), stopwatch.Elapsed.Seconds.ToString("00"));
        milliSecTextField.text = stopwatch.Elapsed.Milliseconds.ToString("000");

        if(stopwatch.Elapsed.Minutes > pitchCount)
        {
            pitchCount++;
            if(AudioManager.instance != null)
            {
                AudioManager.instance.originalSource.pitch = Mathf.Min(2,AudioManager.instance.originalSource.pitch + pitchStep);
            }
        }
    }

    public void ResumeTimer()
    {
        stopwatch.Start();
        milliSecTextField.gameObject.SetActive(true);
    }

    public void StopTimer()
    {
        stopwatch.Stop();
        milliSecTextField.gameObject.SetActive(false);
    }
}
