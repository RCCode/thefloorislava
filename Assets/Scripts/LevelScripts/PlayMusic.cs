﻿using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    [SerializeField] AudioSource source;

    void Start()
    {
        AudioManager.instance?.SetBackgroundMusic(source);
    }
}
