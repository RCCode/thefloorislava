﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class SetLevelScore : MonoBehaviour
{
    void Start()
    {
        GetComponent<TextMeshProUGUI>().text = ScoreManager.instance?.GetScore();
    }
}
