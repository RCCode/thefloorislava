﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] Vector3 offset;
    [Space]

    [Range(0, 10)]
    [SerializeField] float smoothSpeed = 0.125f;
    [SerializeField] float lockYAxis = 0;
    [SerializeField] float lockUpperYAxis = 2;

    Vector3 bufferVector;

    void LateUpdate()
    {
        bufferVector = Vector3.Lerp(transform.position,  target.position + offset, smoothSpeed * Time.deltaTime);

        if(bufferVector.y >= lockYAxis && bufferVector.y <= lockUpperYAxis)
        {
            transform.position = bufferVector;
        }

    }
}
