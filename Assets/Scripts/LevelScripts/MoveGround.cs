﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveGround : MonoBehaviour
{
    [SerializeField] float timeUntilDestroy = 60f;
    [SerializeField] float forwardSpeed = 2.5f;


    Vector3 bufferPos;

    void OnEnable()
    {
        Invoke(nameof(Destroy), timeUntilDestroy);
    }

    void Destroy()
    {
        gameObject.SetActive(false);
    }

    void OnDisable()
    {
        CancelInvoke();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        bufferPos = transform.position;
        bufferPos.x -= forwardSpeed * Time.fixedDeltaTime;

        transform.position = bufferPos;
    }
}
