﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class LavaDeath : MonoBehaviour
{
    [SerializeField] LevelChanger levelChanger;
    [SerializeField] Timer timer;
    [SerializeField] AudioSource deathEffect;
    [Space]
    [SerializeField] float timeToDie = 3f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        timer.StopTimer();
        ScoreManager.instance?.SetScore(timer.stopwatch);

        if(AudioManager.instance != null)
        {
            AudioManager.instance.originalSource.pitch = 1;
            AudioManager.instance.PitchFade();
        }

        deathEffect.Play();
        Invoke(nameof(DeathScreen), timeToDie);
    }

    void DeathScreen()
    {
        levelChanger.FadeToLevel(Scene.DeathScene);
    }
}
