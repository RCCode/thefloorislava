﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler instance;

    [SerializeField] GameObject[] objectsToPool;
    [SerializeField] int objectAmountForEachObject = 10;
    [SerializeField] private bool dynamicPool = true;

    [HideInInspector]
    public Dictionary<int, List<GameObject>> objectPool;

    void Awake()
    {
        if (instance != null)
            Destroy(gameObject);

        instance = this;
    }

    void Start()
    {
        objectPool = new Dictionary<int, List<GameObject>>();
        FillPool();
    }

    void FillPool()
    {
        List<GameObject> bufferList;

        for (int l = 0; l < objectsToPool.Length; l++)
        {
            bufferList = new List<GameObject>();

            for (int i = 0; i < objectAmountForEachObject; i++)
            {
                bufferList.Add(InstantiateObject(objectsToPool[l]));
            }

            objectPool.Add(l, bufferList);
        }
    }

    GameObject InstantiateObject(GameObject obj)
    {
        obj = Instantiate(obj);
        obj.SetActive(false);
        obj.transform.parent = transform;
        return obj;
    }

    public GameObject GetInactiveObject(int index = 0)
    {
        for (int i = 0; i < objectPool[index].Count; i++)
        {
            if(!objectPool[index][i].activeInHierarchy)
            {
                return objectPool[index][i];
            }
        }

        if(dynamicPool)
        {
            objectPool[index].Add(InstantiateObject(objectsToPool[index]));

            return objectPool[index].Last();
        }

        return null;
    }
}
