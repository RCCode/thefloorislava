﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomObjectSpawner : MonoBehaviour
{
    [SerializeField] Transform[] spawnPositions;

    [Space]

    [Range(0, 10)]
    [SerializeField] float minTime;

    [Range(0, 10)]
    [SerializeField] float maxTime;

    [Space]
    [SerializeField] int repeatSpawn = 3;


    int lastObjectIndex = -1;
    int lastObjectPosition = -1;

    bool waitingObject = false;

    // Start is called before the first frame update
    void Start()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
    }

    // Update is called once per frame
    void Update()
    {
        if(!waitingObject)
        {
            StartCoroutine(DoAfterTime(Random.Range(minTime, maxTime)));
        }
    }

    IEnumerator DoAfterTime(float time)
    {
        waitingObject = true;
        while (time > 0)
        {
            time -= Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        for (int i = 0; i < repeatSpawn; i++)
        {
            EnableObject();
        }

        waitingObject = false;
    }

    void EnableObject()
    {
        GameObject obj = ObjectPooler.instance.GetInactiveObject(
            lastObjectIndex =
            GetRandomExcept(
                0, 
                ObjectPooler.instance.objectPool.Count, 
                lastObjectIndex
            ));

        if (obj == null)
            return;

        obj.transform.position = spawnPositions[
            lastObjectPosition =
            GetRandomExcept(
                0, 
                spawnPositions.Length, 
                lastObjectPosition
            )].position;

        obj.SetActive(true);
    }

    /// <summary>
    /// Returns a random Number except one Number
    /// </summary>
    /// <param name="min">Inclusive Min</param>
    /// <param name="max">Exclusive Max</param>
    /// <param name="except">Except this number</param>
    /// <returns></returns>
    int GetRandomExcept(int min, int max, int except)
    {
        int number = Random.Range(min, max);

        if (number.Equals(except))
        {
            if(number.Equals(min))
            {
                number++;
            }
            else if(number.Equals(max - 1))
            {
                number--;
            }
            else
            {
                number++;
            }
        }

        return number;
    }
}
