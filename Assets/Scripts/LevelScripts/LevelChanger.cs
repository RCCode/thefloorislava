﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Transition between Scenes
/// </summary>
public class LevelChanger : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] string triggerName = "FadeOut";

    Scene loadToScene;

    /// <summary>
    /// Make the Fade transition between Scenes
    /// </summary>
    /// <param name="index">Name of Scene</param>
    public void FadeToLevel(Scene index)
    {
        CheckGameRunning();
        ShowCursor();

        loadToScene = index;

        animator.SetTrigger(triggerName);
    }

    /// <summary>
    /// Unpause Game if game was paused
    /// </summary>
    void CheckGameRunning()
    {
        if (Time.timeScale < 1f)
        {
            Time.timeScale = 1f;
        }
    }

    void ShowCursor()
    {
        //CursorLockMode.None fixes locked jiggle Cursor.
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    /// <summary>
    /// Loads the Scene on Animation event
    /// </summary>
    void LoadLevel()
    {
        try
        {
            SceneLoader.instance.LoadScene(loadToScene);
        }
        catch (System.NullReferenceException)
        {
            Debug.LogWarning(
                string.Concat(
                    "NullReferenceException: Scene \'",
                    loadToScene,
                    "\' could not be loaded or SceneLoader has no instance \n -> check if it is loaded as the first Gameobject"
                ));
        }
    }
}
