﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class CharacterController2D : MonoBehaviour
{
    [SerializeField] string jumpString = "Jump";
    [SerializeField] string blinkTriggerString = "Blink";
    [SerializeField] string jumpTriggerString = "Jump";

    [Space]
    [SerializeField] float forceMultiplier = 2;


    Animator anim;
    Rigidbody2D rb;
    AudioSource charAudio;
    bool wantsToJump = false;
    bool isGrounded = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        charAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown(jumpString) && isGrounded)
        {
            wantsToJump = true;
            isGrounded = false;
        }
    }

    void FixedUpdate()
    {
        if(wantsToJump)
        {
            Jump();
            wantsToJump = false;
        }
    }

    public void Jump()
    {
        anim.SetTrigger(jumpTriggerString);
        charAudio.Play();
        rb.AddForce(transform.up * forceMultiplier, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        anim.SetTrigger(blinkTriggerString);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(!isGrounded)
        {
            isGrounded = true;
        }
    }



    private void OnCollisionExit2D(Collision2D collision)
    {
        isGrounded = false;
    }
}
