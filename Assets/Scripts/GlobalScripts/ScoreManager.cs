﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    string score;

    void Awake()
    {
        if (instance != null)
            Destroy(this.gameObject);

        instance = this;

        DontDestroyOnLoad(gameObject);

        score = "00X00X000";
    }

    public void SetScore(Stopwatch s)
    {
        //Missing logic to save score.
        score = string.Join("X", s.Elapsed.Minutes.ToString("00"), s.Elapsed.Seconds.ToString("00"), s.Elapsed.Milliseconds.ToString("000"));
    }

    public string GetScore()
    {
        return score;
    }
}
