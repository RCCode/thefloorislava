﻿public enum Scene
{
    StartGame,
    MenuScene,
    OptionScene,
    LevelScene,
    DeathScene,
    Quit
}

