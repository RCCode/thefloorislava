﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioSource originalSource;

    private void Awake()
    {
        if (instance != null)
            Destroy(this.gameObject);

        instance = this;
        DontDestroyOnLoad(gameObject);

    }

    public void StopBackgroundMusic()
    {
        originalSource.Stop();
    }

    /// <summary>
    /// Fade Out the AudioListener
    /// </summary>
    public void ToggleAudio(float duration = 0.02f)
    {
        StartCoroutine(FadingOut(duration));
    }

    public void SetBackgroundMusic(AudioSource SceneSource)
    {
        if (originalSource.clip != SceneSource.clip || !originalSource.isPlaying || originalSource.pitch != SceneSource.pitch)
        {
            originalSource.pitch = SceneSource.pitch;
            originalSource.clip = SceneSource.clip;
            originalSource.timeSamples = SceneSource.timeSamples;
            originalSource.Play();
        }
    }

    public void PitchFade(float duration = 2f)
    {
        StartCoroutine(FadePitch(duration));
    }

    /// <summary>
    /// Fade Out every Sound on Scenechange
    /// </summary>
    IEnumerator FadingOut(float duration)
    {
        while (AudioListener.volume > 0)
        {
            AudioListener.volume -= Time.deltaTime / duration;


            yield return new WaitForEndOfFrame();
        }

        AudioListener.volume = 1;
    }

    IEnumerator FadePitch(float duration)
    {
        float originalVolume = originalSource.volume;
        float originalPitch = originalSource.pitch;

        while (originalSource.volume > -30 && originalSource.pitch > -1)
        {
            originalSource.volume -= Time.deltaTime / duration;

            originalSource.pitch -= Time.deltaTime / duration;

            yield return new WaitForEndOfFrame();
        }

        originalSource.volume = originalVolume;
        originalSource.pitch = originalPitch;


    }
}

