﻿using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// Loads a Scene by an Enum
/// </summary>
public class SceneLoader : MonoBehaviour
{
    public static SceneLoader instance;

    void Awake()
    {
        if (instance != null)
            Destroy(this.gameObject);

        instance = this;

        DontDestroyOnLoad(gameObject);

        Invoke(nameof(StartGame), 1);
    }

    void StartGame()
    {
        LoadScene(Scene.MenuScene);
    }

    public void LoadScene(Scene scene)
    {
        if (scene == Scene.Quit)
            QuitApplication();
        else
        {
            AudioManager.instance.ToggleAudio();
            SceneManager.LoadSceneAsync((int)scene, LoadSceneMode.Single);
        }
    }

    void QuitApplication()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}

